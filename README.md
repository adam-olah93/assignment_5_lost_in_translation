# Sign Language Translator

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Web application** with **React** framework.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This application is an online sign language translator. Users can enter a username, then type in words and translate them to sign language pictures. On their profile page they can see their last ten translations and they can clear their translation history.

The application is deployed via Heroku and can be accessed at https://sign-translator-assignment.herokuapp.com/

## Install

- install Node.js
- clone repository
- run ```npm install``` in the root folder

## Usage

To start the application locally, run ```npm run dev```

The application is deployed via Heroku and can be accessed at https://sign-translator-assignment.herokuapp.com/

## Maintainers

- [Laszlo Laki](https://gitlab.com/lakilukelaszlo)
- [Adam Olah](https://gitlab.com/adam-olah93)
- [Krisztina Pelei](https://gitlab.com/kokriszti)
