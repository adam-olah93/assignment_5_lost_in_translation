import { useForm } from "react-hook-form"
import {loginUser} from "../../api/userApi";
import {useEffect, useState} from "react";
import {storageSave} from "../../utils/storage";
import { useNavigate } from "react-router-dom"
import {useUser} from "../../context/UserContext";
import {FaArrowCircleRight, FaRegKeyboard} from "react-icons/fa"
import logo from './Logo.png';

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    //Hooks
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { user, setUser } = useUser();                                    //from context
    const navigate = useNavigate();

    //Local state
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)

    //Side effects
    useEffect(() => {
        console.log("User has changed: ", user)
        if(user !== null) navigate("translator")
    }, [user, navigate])

    //Event handlers
    const onSubmit = async (data) => {
        setLoading(true)                        //while loading, button is disabled, displays "Loading..."
        console.log(data)
        const [error, userFromServer] = await loginUser(data.username)
        console.log("Error: ", error)
        console.log("User: ", userFromServer)
        if(error !== null) setApiError(error)
        if(userFromServer !== null) {
            storageSave("user", userFromServer)
            setUser(userFromServer)
        }
        setLoading(false)
    }

    console.log("Login validatation error: ", errors)

    //Render functions
    const errorMessage = (() => {
        if(!errors.username) return null;

        if(errors.username.type === "required") {
            return <span>Username is required</span>
        }

        if(errors.username.type === "minLength") {
            return <span>Username must be at least 3 characters</span>
        }
    })();

    return (
        <div className="main-container">
            <div className="hero-container">
                <div className="hero-img-container">
                    <svg className="hero-img-splash" width="295" height="223" viewBox="0 0 295 223" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M94.7158 61.5125C81.5158 74.7125 57.2159 61.5125 33.2158 76.5125C-27.2842 110.513 3.7157 207.013 55.7158 191.013C122.758 170.384 144.216 218.013 228.216 222.513C312.216 227.013 305.716 118.513 265.716 109.013C212.452 96.3623 257.716 35.5125 194.716 7.01252C131.716 -21.4875 111.216 45.0125 94.7158 61.5125Z" fill="white"/>
                    </svg>
                    <img src={logo} className="hero-img-robot" alt="logo"/>
                </div>
                <div className="hero-text-container">
                    <h1 className="hero-title">Lost in Translation</h1>
                    <h3 className="hero-subtitle">Get started</h3>
                </div>
            </div>
            <div className="login-input-box">
                <form onSubmit={ handleSubmit (onSubmit)}>
                    <div className="login-input-container">
                        <FaRegKeyboard className="keyboard-icon" />
                        <span className="divider"> | </span>
                        <input
                            type="text"
                            placeholder="What's your name?"
                            className="login-input-field"
                            {...register("username", usernameConfig)}
                        />

                        <button type="submit" disabled={ loading } className="arrow-button"><FaArrowCircleRight className="arrow-icon"/></button>
                    </div>
                    { errorMessage }
                    { loading && <p>Logging in...</p> }
                    { apiError && <p>{ apiError }</p> }
                </form>
            </div>

        </div>
    );
};

export default LoginForm;
