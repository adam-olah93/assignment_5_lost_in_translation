const SignLanguage = ({ characters }) => {
  return (
    <div>
      {characters.map((char, index) => {
        return (
          <img
            key={index}
            width="80px"
            height="80px"
            src={require("./SignImg/" + char + ".png")}
            alt="signimage"
          />
        );
      })}
    </div>
  );
};

export default SignLanguage;
