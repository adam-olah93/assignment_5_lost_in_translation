import { useState } from "react";
import { storageSave } from "../../utils/storage";
import { translationAdd } from "../../api/translationApi";
import { useUser } from "../../context/UserContext";
import { FaArrowCircleRight, FaRegKeyboard } from "react-icons/fa";

const TranslatorForm = ({ setCharacters }) => {
  // States
  const { user, setUser } = useUser();
  const [inputString, setInputString] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(true);

  // Event Handlers
  const changeHandler = (event) => {
    // If input field contains only white-space, the translation button is disabled
    if (event.target.value.trim().length === 0) {
      setButtonDisabled(true);
    } else {
      setButtonDisabled(false);
      // Sets input value to state
      setInputString(event.target.value.trim());
    }
  };

  const submitHandler = (event) => {
    event.preventDefault();

    let filteredArr = Array.from(inputString) // Creates an array
      .map((char) => char.toLowerCase()) // Forms lowercase
      .filter((char) => char.match(/[a-z]/)); // Finds English alphabet letters

    // Calls API to add new translation data to endpoint, and saves data to local Session Storage
    fetch(`${process.env.REACT_APP_API_URL}?id=` + user.id)
      .then((res) => res.json())
      .then((data) => {
        let newTranslations = data[0].translations;
        newTranslations.push(inputString);

        // Saves to Session Storage
        storageSave("user", {
          ...user,
          translations: newTranslations,
        });
        // Adds new translation to endpoint
        translationAdd(user, inputString);

        // Sets new translations to user
        setUser({ ...user, translations: newTranslations });

        // Sets state function from parent
        setCharacters(filteredArr);
      });
  };

  return (
    <form onSubmit={submitHandler}>
        <label htmlFor="translation-input">Words to translate: </label>
        <div className="translation-input-container">
          <FaRegKeyboard className="keyboard-icon" />
          <span className="divider"> | </span>
          <input
            maxLength={40}
            onChange={changeHandler}
            type="text"
            placeholder="Hello! type something..."
            className="translation-input-field"
          />
          <button type="submit" disabled={buttonDisabled}  className="arrow-button">
            <FaArrowCircleRight  className="arrow-icon"/>
          </button>
        </div>
    </form>
  );
};

export default TranslatorForm;
