import { storageDelete, storageSave } from "../../utils/storage";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { translationClearHistory } from "../../api/translationApi";


const ProfileDeletion = () => {

  const { user, setUser } = useUser();

  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis cannot be undone!")) {
      return;
    }
    const [clearError] = await translationClearHistory(user);

    if (clearError !== null) {
      return;
    }


    const updatedUser = {
      ...user,
      translations: [],
    };

    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  const handleLogoutClick = () => {
    if(window.confirm('Are you sure?')){
      storageDelete(STORAGE_KEY_USER)
      setUser(null)
       
    }
  }

  return (
    <div className="profile-btn-container">
      <button className="profile-btn" onClick={handleClearHistoryClick}>Clear history</button>
        <button className="profile-btn" onClick={ handleLogoutClick }>Logout</button>
    </div>
  );
};
export default ProfileDeletion;
