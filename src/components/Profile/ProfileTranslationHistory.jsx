
import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";
import ProfileDeletion from "./ProfileDeletion";
import { useUser } from "../../context/UserContext";

const ProfileTranslationHistory = ({ translations }) => {
  const { user, setUser } = useUser();

  const translationList = translations.map((translation, index) => (
    <ProfileTranslationHistoryItem
      key={index + "-" + translation}
      translation={translation}
    />
  ));

  return (
    <section>
        <ProfileDeletion/>
      <section className="translation-history-container">
        {translationList.length !== 0 && (
          <h4  className="translation-history-title">Your translation history</h4>
        )}
        {translationList.length === 0 && (
          <h4  className="translation-history-title">You have no translations.</h4>
        )}
        <ul>{translationList}</ul>
      </section>

      </section>
  );
};
export default ProfileTranslationHistory;
