

const ProfileHeader = ({ username }) => {
  return (
      <header>
        <h2 className="profile-headline"> Welcome {username}!</h2>
      </header>
  );
};
export default ProfileHeader;
