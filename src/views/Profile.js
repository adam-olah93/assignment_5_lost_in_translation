import ProfileDeletion from "../components/Profile/ProfileDeletion";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";

const Profile = () => {

  const { user } = useUser();

  return (
    <div className="profile-container">
      <ProfileHeader username={user.username} />
      <ProfileTranslationHistory translations={user.translations.slice(-10)} />
    </div>
  );
};

export default withAuth(Profile);
