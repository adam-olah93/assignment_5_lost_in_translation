import withAuth from "../hoc/withAuth";
import { useState } from "react";
import SignLanguage from "../components/Translator/SignLanguage";
import TranslatorForm from "../components/Translator/TranslatorForm";

const Translator = () => {
  const [characters, setCharacters] = useState([]);

  return (
    <div>
      {
        <>
          <section id="translation-form">
              <TranslatorForm setCharacters={setCharacters} />
          </section>
          {characters.length > 0 && <section  className="translation-result-container">
            <SignLanguage characters={characters} />
                                </section>}
        </>
      }
    </div>
  );
};

export default withAuth(Translator);
